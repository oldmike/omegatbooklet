#!/bin/bash

#set -e

for ((i=4; i>0; i--))
do
    echo $i
    pdflatex -shell-escape omegat.tex
done
