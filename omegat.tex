\documentclass[a4paper]{scrreprt}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[lowtilde]{url}
\usepackage[breaklinks=true]{hyperref}
\usepackage{lmodern}
\usepackage[doublespacing]{setspace}
\let\tempone\itemize
\let\temptwo\enditemize
\renewenvironment{itemize}{\tempone\addtolength{\itemsep}{-0.9\baselineskip}}{\temptwo}
\usepackage{graphicx}
% Der Befehl figure wurde so umdefiniert, dass auch ohne float ein Bild 
% an der Stelle angezeigt wird, an der es eingefügt ist.
\makeatletter
\renewenvironment{figure}[1][]{
\par
\medskip
\begin{minipage}{\linewidth}
\def\@captype{figure}
}{
\end{minipage}
\medskip
\par
}
\makeatother
\makeatletter
\renewcommand*\l@figure{\@dottedtocline{1}{1.5em}{3em}}
\makeatother
\usepackage[ngerman]{babel}

\begin{document}

% Titlepage
\begin{titlepage}
\pagenumbering{Roman}
\centering
{\huge\bfseries "`Literate Programming"'\par}
{\Large\bfseries und\par}
{\huge\bfseries OmegaT}
\vfill
{\Large\itshape Dipl.-Ing. Mechtilde Stehmann\par}
und\par
{\Large\itshape Dr. Michael Stehmann}
\vfill
{\large \today\par}
\thispagestyle{empty}
\end{titlepage}

% TableOfContent
\renewcommand{\contentsname}{Inhalt}
\tableofcontents
\cleardoublepage
\pagenumbering{arabic}
\setcounter{page}{1}

% Text
\chapter{Vorwort}

Für die Übersetzung unseres Buches \texttt{Pakete bauen mit Git-Buildpackage}
\footnote{\url{https://ddp-team.pages.debian.net/dpb/BuildWithGBP.pdf}}
hat sich \texttt{OmegaT}\footnote{\url{https://packages.debian.org/sid/omegat}}
als ein nützliches Werkzeug erwiesen, obwohl es für "`Literate Programming"'
\footnote{\url{http://www.literateprogramming.com/knuthweb.pdf}}
nicht speziell ausgelegt ist.

\texttt{OmegaT} ist ein \textit{Computer Assisted Translation} (CAT) Werkzeug.

Das Programm unterstützt die Übersetzenden durch Arbeitsblätter, auf denen
die Ausgangstexte satz- oder segmentweise in die Zielsprache übertragen werden.
Farbliche Markierungen sorgen für Übersicht.

Beide Teile werden anschließend in einen Übersetzungsspeicher
(\textit{translation memory}) gespeichert, aus welchem dann
Übersetzungsvorschläge fließen. Der Übersetzungsspeicher wird für jede
Sprachrichtung getrennt angelegt und in einem standardisierten
Austauschformat gespeichert.

Es gibt Glossare, in denen auch unscharf nach Einzelworten oder -phrasen
gesucht werden kann, und Rechtschreibprüfungen. Die Glossardateien sind
reine Textdateien.

Die Arbeit mit diesem Werkzeug soll im Folgenden knapp dargestellt werden,
um eine Einführung in die Nutzung zu geben. Dabei wird auch auf die besonderen 
Probleme (s. Kapitel~\ref{sec:problems}, Seite~\pageref{sec:problems})
eingegangen, die sich beim Übersetzen von "`Literate Programming"'-Dokumenten
gezeigt haben.

"`Literate programming"' bedeutet technisch gesehen, dass sowohl die
Dokumentation als auch der Quelltext des Programms in einer gemeinsamen
Datei vorhanden sind. Spezielle Werkzeuge sorgen dafür, dass sowohl der
Quelltext als auch die Dokumentation aus dieser Datei extrahiert werden
können.\footnote{\url{https://de.wikipedia.org/wiki/Literate\_programming}}

Das Werkzeug, das wir hierzu nutzen, ist \texttt{noweb}.
\footnote{\url{https://de.wikipedia.org/wiki/Noweb}}

Obwohl es eine umfangreiche Dokumentation zu \texttt{OmegaT} gibt
(s. Kapitel~\ref{chap:documentation}, Seite~\pageref{chap:documentation}),
erscheint es sinnvoll, die besonderen Probleme (s. Kapitel~\ref{sec:problems},
 Seite~\pageref{sec:problems}), die sich beim Übersetzen von
"`Literate Programming"'-Dokumenten ergeben, und deren Lösung zu behandeln.

Es geht im Folgenden somit vor allem um die Übersetzung von
\textit{.tex}-Dateien, die aus \textit{.nw}-(noweb-)Dateien entstanden
sind.

Zuvor soll jedoch in aller Kürze die Einrichtung der Arbeitsumgebung
(Kapitel~\ref{chap:environment}, Seite~\pageref{chap:environment}) und der 
Übersetzungsprozess Kapitel~\ref{chap:translation},
Seite~\pageref{chap:translation}) darzgestellt werden.

Die praktischen Beispiele vor allem zur Organisation sind der oben genannten
Übersetzungsarbeit entnommen, können aber unschwer an andere Aufgaben
angepasst werden.

Die Verfasser hoffen, dass die folgenden summarischen Ausführungen den
Einstieg in die Arbeit mit \texttt{OmegaT} erleichtern und bitten um
Übersetzungen dieses Textes.

\chapter{Lizenz}

Der Text dieses Buches  von Mechtilde und Michael Stehmann steht unter
der \textbf{Creative Commons Namensnennung - Weitergabe unter gleichen
Bedingungen 4.0 International Lizenz (CC BY-SA 4.0)}
.\footnote{\url{https://creativecommons.org/licenses/by-sa/4.0/legalcode}}

\chapter{Dokumentation}
\label{chap:documentation}

Für \texttt{OmegaT} gibt es unter \textit{http://omegat.org/<ISO-Code>/documentation}
eine umfangreiche Dokumentation in zahlreichen Sprachen mit einem
Handbuch, Anleitungen, Videos und Hinweisen zu weiteren Dokumenten. Es
gibt sie dort auch in deutscher
Sprache\footnote{\url{http://omegat.org/de/documentation}}.

Ferner gibt es unter \textit{/usr/share/doc/omegat/html/}
Dokumentation in vielen Sprachen.

\chapter{Einrichtung der Arbeitsumgebung}
\label{chap:environment}

\section{Benötigte \texttt{Debian}-Pakete}

Es sind folgende \texttt{Debian}-Pakete zu installieren:

\begin{itemize}

\item noweb
\item texlive
\item texlive-bibtex-extra
\item texlive-binaries
\item texlive-extra-utils
\item texlive-lang-german
\item texlive-lang-japanese
\item texlive-latex-extra
\item biber

\end{itemize}

Und für die Übersetzungsarbeit:

\begin{itemize}

\item omegat
\item texlive-lang-<your language>

\end{itemize}

Wenn man  die Upstream-Version von \texttt{OmegaT} installiert, ist auch
das Paket \textit{hunspell} zu installieren.

\section{Arbeitsverzeichnis}
\label{sec:workdir}

Ferner ist ein Arbeitsverzeichnis anzulegen, in das in unserem Fall das
\texttt{Git}-Repositorium des Buches geklont wird.

Dann wird in dieses Verzeichnis gewechselt.

In unserem Beispiel sind also folgende Befehle auszuführen:

\begin{verbatim}
git clone https://salsa.debian.org/ddp-team/dpb
cd dpb
\end{verbatim}

Durch die Ausführung des Skriptes \textit{create-book.sh} können die zur
Übersetzung benötigten \textit{.tex}-Dateien aus den \textit{.nw}-Dateien
erzeugt werden.

In anderen Fällen ist ein Arbeitsverzeichnis anzulegen, und dort sind dann 
alle Originaldateien einzufügen. Dann wird in dieses Verzeichnis gewechselt.

\section{Verzeichnisse für die Übersetzung}
\label{sec:translation_dirs}

Das Verzeichnis \textit{translation/<ISO-Code>/} ist das Arbeitsverzeichnis
für die Übersetzungen der jeweiligen Sprache. Wenn es noch nicht existiert,
ist dieses Verzeichnis anzulegen.

Die deutschen \textit{.tex}-Dateien können erzeugt werden, indem das Skript
\textit{create-book.sh}, welches in unserem Buch \texttt{Pakete bauen
mit Git-Buildpackage} dokumentiert ist, ausgeführt wird. Die Dateien
befinden sich dann direkt im Verzeichnis \textit{dpb/}.

Dieser Schritt ist notwendig, weil nicht \textit{.tex}-Dateien
das zu übersetzende Original sind, sondern die \textit{.nw}-Dateien.

Alternativ können die  \textit{.tex}-Dateienauch aus dem Web
heruntergeladen werden. Hierzu ist im Browser
\textit{https://ddp-team.pages.debian.net/dpb/de/<Dateiname>.tex}
einzugeben. \textit{curl} und \textit{wget} erledigen diese Aufgabe auf
der Kommandozeile.

Mit \texttt{OmegaT} kann dann das Kopieren der \textit{.tex}-Dateien in das 
Verzeichnis \textit{translation/ <ISO-Code>/source/} erfolgen (s.
Kapitel~\ref{sec:translation_start}, Seite~\pageref{sec:translation_start}).

Im Verzeichnis \textit{translation/<ISO-Code>/target/} werden später die
übersetzten \textit{.tex}-Dateien abgelegt. In diesem Verzeichnis sind
gegebenfalls symbolische Verknüpfungen zu erstellen (s.
Kapitel~\ref{sec:links}, Seite~\pageref{sec:links}).

Im Verzeichnis \textit{translation/<ISO-Code>/} befinden sich neben den
beiden genannten (Unter-)Verzeichnissen weitere Dateien, die als Teil des
Projektes ebenfalls zu versionieren sind.

\section{Ein neues Projekt in \texttt{OmegaT} anlegen}

\texttt{OmegaT} organisiert die Übersetzungsarbeit in sogenannten
Übersetzungsprojekten.

Jedes Projekt benötigt ein zunächst ein Verzeichnis. Dieses muss zunächst
leer sein. In unserem Fall ist dies das Verzeichnis
\textit{translation/<ISO-Code>/}.

\texttt{OmegaT} erzeugt in jedem Projektverzeichnis eine Reihe von
(Unter-)Verzeichnissen und Dateien.
 
In diesen werden die Quelldokumente, die zu übersetzen sind, die Glossare
und die \textit{translation memories}, die man benutzen will, gespeichert.
\texttt{OmegaT} erzeugt auch einen Zielordner, der für übersetzte
Dokumente bestimmt ist. 

Startet man \texttt{OmegaT}, so wird eine Schnelleinführung angezeigt,
in welcher im ersten Kapitel erklärt wird, wie man ein neues Projekt anlegt.

\begin{figure}[h]
\centering
\includegraphics[width=30em]{pictures/omegat_start.png}
\caption{OmegaT gestartet}
\end{figure}

Man findet im Menü \textit{Projekt} den Eintrag \textit{Neu ...},
welcher auszuwählen ist. Es erscheint ein Dateiauswahldialog.

\begin{figure}[h]
\centering
\includegraphics[width=30em]{pictures/neues_projekt_erstellen.png}
\caption{OmegaT - Neues Projekt erstellen}
\end{figure}

Das Verzeichnis, in dem das neue Projekt angelegt werden soll,
ist nach dem ISO-Code der Sprache, in die übersetzt werden soll, benannt.
Es muss zunächst leer sein. Nach Auswahl dieses Verzeichnisses erscheint
ein Dialog, in welchem das Projekt spezifiziert werden kann.

\begin{figure}[h]
\centering
\includegraphics[width=30em]{pictures/angaben_zum_neuen_projekt.png}
\caption{Angaben zur Spezifikation des neuen Projekts}
\end{figure}

In diesem Dialog ist der Haken bei "`Satzsegmentierung aktivieren"' zu
entfernen, um die Satzsegmentierung zu deaktivieren. Dann wird pro Absatz
(und nicht für jeden Satz) ein Segment gebildet und im Editor-Fenster angezeigt.

\section{Nützliche Verknüpfungen}
\label{sec:links}

Es gibt einige Dateien, die zur Erstellung des (\textit{.pdf}-)Dokumentes
benötigt werden, aber regelmäßig keiner Übersetzung bedürfen.

Hierbei handelt es sich beispielsweise um Bilder, aber auch um die
Literaturdatenbank.

Es ist sinnvoll, insoweit unnötige Doppelung zu vermeiden. Außerdem ist
es gut, von diesen Dateien stets die aktuelle Version zu verwenden.

Dies kann durch symbolische Links im Verzeichnis
\textit{translation/<ISO-Code>/target/} auf die Dateien oder Verzeichnisse
des Orignals erreicht werden. Im Falle des eingangs erwähnten Buchprojektes
können diese mit folgenden Befehlen erstellt werden:

\begin{verbatim}
cd <path>/translation/<ISO-Code>/target/
ln -s <path>/dpb/Pictures Pictures
ln -s <path>/dpb/Literatur.bib Literatur.bib
\end{verbatim}

\section{Bewährte Einstellungen}

Durch \textit{Optionen} -> \textit{Einstellungen} gelangt man in den
Einstellungsdialog.

Unter \textit{Darstellung} \textit{Schriftart} sollte \texttt{Monospaced}
gewählt werden. Eine solche Schriftart ist gegebenenfalls vorab zu
installieren.

Eine Einstellung im Bereich \textit{Editor} kann bei neueren Versionen
des Upstream-Projektes notwendig werden (s. Kapitel~\ref{subsec:tecproblems},
Seite~\pageref{subsec:tecproblems}).

\chapter{Übersetzen mit \texttt{OmegaT}}
\label{chap:translation}

\section{Start mit \texttt{OmegaT}}
\label{sec:translation_start}

Nach der Auswahl von \textit{Projekt -> Öffnen} erscheint wieder ein
Dateiauswahldialog.

Nach Auswahl des Übersetzungsprojektes anhand der Zielsprache erscheint
folgender Dialog:

\begin{figure}[h]
\centering
\includegraphics[width=30em]{pictures/projektdateien.png}
\caption{Projektdateien}
\end{figure}

Die zu übersetzenden Dateien sind in den Quellordner zu kopieren.
Dies können die deutschen Originaldateien sein oder auch die
\textit{.tex}-Dateien einer (möglichst vollständig übersetzten) anderen
Sprache.

Die Dateien sind über einen Dateiauswahldialog einzeln zu kopieren. Sie
werden von \texttt{OmegaT} im Verzeichnis \textit{translation/<ISO-Code>/source/}
abgelegt.

Im Verzeichnis \textit{translation/<ISO-Code>/target/} werden später die
übersetzten \textit{.tex}-Dateien abgelegt.

\section{Übersetzungsprozess}

Der Übersetzungsprozess hat folgende Schritte:

Die Formulierung des Originals in deutscher Sprache im Format noweb
(\textit{.nw}) ist Aufgabe der Autoren.

Zunächst ist auch durch die Übersetzenden eine entsprechende Umgebung
einzurichten (s.
Kapitel~\ref{chap:environment}, Seite~\pageref{chap:environment}).

Dann beginnt die Arbeit der Übersetzenden:

\begin{itemize}\itemsep2pt

\item Umwandlung der \textit{.nw}-Dateien mit \textit{noweave} in
\textit{.tex}-Dateien; dies kann - wie dargelegt (Kapitel~\ref{sec:workdir},
Seite~\pageref{sec:workdir}) - mit dem Skript \textit{create-book.sh}
erfolgen.
\item Die deutschen \textit{.tex}-Dateien sind die Quelle zumindest für
die engliche Übersetzung.
\item Die Dateien sind nach \textit{translation/<ISO-Code>/source/} zu
kopieren (Kapitel~\ref{sec:translation_dirs}, Seite~\pageref{sec:translation_dirs}).
\item Sie werden einzeln mit \texttt{OmegaT} übersetzt.
\item Die Übersetzung wird im Verzeichnis \textit{translation/<ISO-Code>/target/}
abgelegt.
\item Die \textit{pdf}-Dateien können mit dem Skript \textit{create-book.sh}
oder einem angepassten Skript gebaut werden.

In diesem oder einem entsprechenden Skript müssen neue Sprachen zunächst
hinzugefügt werden, indem die entsprechende Liste (\textit{LANGS=" ... "})
am Beginn des Skriptes ergänzt wird.

\end{itemize}

\section{Übersetzungsarbeit}

Die eigentliche Übersetzungsarbeit erfolgt in einem dreigeteilten Fenster.

Auf der linken Seite befindet sich der "`Editor"', in den die Übersetzung
eingegeben wird.

Auf der rechten Seite oben befindet sich das "`Treffer"'-Fenster, in
welchem Treffer aus dem \textit{translation memory} (TM) angezeigt werden.
Darunter ist das "`Glossar"'-Fenster, das Übereinstimmungen mit dem Glossar
anzeigt.

Im Editor-Bereich werden die Quelltexte, in einzelne Sätze (oder Absätze)
segmentiert, angezeigt. Diese Segmente werden nacheinander übersetzt.

Wenn man vom übersetzten Segment zum nächsten übergeht, wird die
Übersetzung im Übersetzungsspeicher (\textit{translation memory})
gespeichert.

Sobald alle Segmente übersetzt worden sind (oder vorher, wenn man das so
will -- s. Kapitel~\ref{sec:export},
Seite~\pageref{sec:export}), wird \texttt{OmegaT} den
\textit{translation memory} verwenden, um das übersetzte Dokument bzw.
die übersetzten Dokumente im Zielordner zu erzeugen. 

\section{Tastaturkürzel}
\label{sec:shortcuts}

Will man nicht zur Maus greifen, ist die Kenntnis folgender Tastaturkürzel
sinnvoll.

\begin{description}

\item[Strg-N] Gehe zum nächsten Segment
\item[Strg-P] Gehe zum vorherigen Segment
\item[Strg-U] Gehe zum nächsten nicht übersetzten Segment
\item[Strg-Shift-U] Gehe zum nächsten übersetzten Segment
\item[Strg-S] Übersetzung speichern
\item[Strg-Shift-S] Übersetzung identisch mit Quelle (z. B. im Falle
von Code-Chunks)
\item[Strg-R] Durch Treffer des Übersetzungsspeichers ersetzen

(\textit{translation memory} übernehmen ist meist bei nur kleineren Änderungen
sinnvoll.)

\item[Strg-Shift-R] Durch Vorschlag ersetzen
\item[Strg-Shift-A] Quelltext markieren
\item[Strg-D] Übersetzte Dateien erstellen
\item[Strg-Shift-D] Aktuelle Datei erstellen

S. a. Kapitel~\ref{sec:export}, Seite~\pageref{sec:export}.

\end{description}

Zeilenumbrüche, beispielsweise in \textit{verbatim}-Abschnitten, können
mit \textbf{\textsf{Shift-Return}} bewirkt werden.

\section{Probleme und Lösungen}
\label{sec:problems}

Spezifische Probleme bei der Übersetzung von Dateien mit Code-Chunks
(aus noweb) ergeben sich aus der Arbeitsweise von \texttt{OmegaT}. Diese
soll den Übersetzenden die Arbeit erleichtern. Nicht zu übersetzende Teile
(z. B. Formatierungen) werden für das Editor-Fenster in Tags (\textless
... \textgreater) umgewandelt, um sie als nicht zu übersetzend kenntlich
zu machen.

Dies wird dann problematisch, wenn solche Zeichen(-ketten) in Code-Chunks
enthalten sind und entsprechend behandelt werden.

\subsection{Formatierung der Code-Chunks}

Das bedeutenste Problem ist folgendes:

Einrückungen und einfache Zeilenumbrüche in Code-Chunks, die noch in der
Quell-\textit{.tex}-Datei vorhanden sind, sind in der \textit{.tex}-Datei,
die die Übersetzung enthält, nicht mehr vorhanden. Bereits im Editor-Fenster
werden diese Formatierungen nicht mehr angezeigt.

Eine befriedigende Lösung für dieses Problem gibt es bislang nicht.

Die Arbeit des Übersetzenden besteht an dieser Stelle leider darin,
die Formatierungen des Originaldokumentes zu rekonstruieren.

Wer eine bessere Lösung hat, möge sich bitte melden.

\subsection{Schwierigkeiten mit dem \&}

In unserem Code kommt häufig folgende Zeichenkette vor:

\begin{verbatim}
... 3>&2 2>&1 1>&3
\end{verbatim}

(Hier unproblematisch, da in einem Verbatim-Abschnitt eingebettet.)

Im Editor-Fenster sieht dies dann etwa wie folgt aus (Beispiel):

\begin{figure}[h]
\centering
\includegraphics[width=30em]{pictures/input_problem.png}
\caption{Problem mit \&}
\end{figure}

Die entsprechenden Segmente können mit \textit{Strg-Shift-S} als
"`Übersetzung identisch mit Quelle"' behandelt werden.

\subsection{Behandlung des \%}

Das Zeichen \% kennzeichnet in \TeX{} den Beginn von Kommentaren. 

Daher wird auch hier der Text vor dem Übersetzer "`versteckt"'.

Auch in Codezeilen kann jedoch ein Prozentzeichen gelegentlich vorkommen.

Das Segment wird dann mit \textit{Strg-Shift-S} behandelt. Vor dem
Prozentzeichen werden von \texttt{OmegaT} allerdings dann noch ein
Leerzeichen und eine Leerzeile eingefügt.

Das letzte Zeichen vor dem \% war ein "`\texttt{\_}"'. Die \textit{.tex}-Datei, die
die Übersetzung enthält, wird daher mit folgendem Befehlzeile behandelt:

\begin{verbatim}
sed --in-place --expression ':a;N;$!ba;s/_ \n\n%/_%/' <Dateiname>.tex
\end{verbatim}

\subsection{Geschweifte Klammern}

Geschweifte Klammern (\{ \}) sind mit oft mit einem Backslash
(\textbackslash) zu maskieren.

\subsection{Einlesen}

Probleme mit dem Einlesen der zu übersetzenden \textit{.tex}-Dateien kann
es geben, wenn diese insgesamt zu groß, der zu bearbeitende Text also zu
umfangreich ist.

Eine Lösung kann dann sein, die Dateien in Verzeichnis
\textit{translation/ <ISO-Code>/source/} (s. Kapitel~\ref{sec:translation_dirs},
Seite~\pageref{sec:translation_dirs}) durch Entfernen oder Verschieben
zu reduzieren. Nur die direkt danach zu bearbeitende Datei muss in diesem
Verzeichnis bleiben.

\subsection{Technische Probleme}
\label{subsec:tecproblems}

Die \texttt{Debian}-Version von \texttt{OmegaT} funktioniert mit den
vorgenannten Einschränkungen ohne Probleme.

Die Versionen des Upstream-Projektes sollten aber ohne JRE installiert werden.
Es muss dann das \texttt{Debian}-Paket \textit{openjdk-17-jdk} nach-
bzw. vorinstalliert werden.

Die Versionen der 6er-Reihe weisen Probleme bei größeren Textteilen mit
vielen Abschnitten auf.

Als Workaround ist in \textit{Optionen -> Einstellungen ...} unter
\textit{Editor} im Feld nach \textit{Diese Anzahl von Segmenten laden} ein Wert
unterhalb des Standardwerts von 2000 einzugeben. Hier hilft Ausprobieren.

\section{Export der Übersetzungen}
\label{sec:export}

Mit dem Tastaturkürzel \textbf{\textsf{Strg}} gedrückt halten und dann
\textbf{\textsf{S}} und \textbf{\textsf{D}} eingeben erfolgt der Export
der Übersetzungen. (Weitere Tastaturkürzel: Kapitel~\ref{sec:shortcuts},
Seite~\pageref{sec:shortcuts}.)

Ins Verzeichnis \textit{translation/<ISO-Code>/target/} werden dann die
übersetzten \textit{.tex}-Dateien abgelegt.

\chapter{Wichtiger Hinweis}

Es ist darauf zu achten, dass sich im Verzeichnis 
\textit{translation/<ISO-Code>/source/} stets aktuelle Dateien befinden.

\chapter{Ergänzungen und Korrekturen im Original}

Ist das zu übersetzende Werk noch "`work in progress"' (wie das eingangs
erwähnte Buch), so sind immer wieder Ergänzungen und Korrekturen in die
Übersetzung einzupflegen.

Der Arbeitsablauf ist folgender:

\begin{itemize}\itemsep2pt

\item Die geänderten \textit{.tex}-Dateien des zu übersetzenden Werkes
sind erneut mit \texttt{OmegaT} nach \textit{translation/<ISO-Code>/source/} zu
kopieren (Kapitel~\ref{sec:translation_dirs},
Seite~\pageref{sec:translation_dirs}).

\item Die geänderten Segmente sind als "`nicht übersetzt"' markiert.

\item Bei kleineren Änderungen sind die Vorschläge des Übersetzungsspeichers
\textit{translation memory} nützlich. Dort werden auch die Änderungen
angezeigt. Man kann dann entscheiden, was zu übernehmen ist.

\item Schließlich sind die neuen Übersetzungen zu exportieren
(Kapitel~\ref{sec:export}, Seite~\pageref{sec:export}).

\item Änderungen bei eingefügten Bildern erfordern nur bei einer Änderung
der Bildunterschrift eine Tätigkeit des oder der Übersetzenden.

\end{itemize}

\end{document}
