# omegatbooklet

## Übersetzen mit OmegaT

is a description how to translate documentation with OmegaT.

It is concerned with the translation esp. of .tex and .nw (noweb) files.

## Authors

are Mechtilde and Michael Stehmann.

It is only in german language now - but translations are welcome.

## Contributing

We are open for contributions esp. translations.

## Licence

[Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

## Download

There is a CI/CD enabled so you can download the pdf as an artifact at

[pages.debian.net](https://oldmike.pages.debian.net/omegatbooklet/omegat.pdf) .
